# Requirements

 - Java or Kotlin knowledge

# Configuration

 - Download repository
 - Unzip file
 - Copy **MoneySpaceSDK** from the unzip folder and paste in project
 - Open **AndroidManifest**
  
**Add internet permission**
```XML
<uses-permission android:name="android.permission.INTERNET" />
```

**Add intent-filter to activity to redirect to the app after payment is completed ( Deep Link )**

- Host : moneyspace
- SchemeActivity : payment
- URL : payment://moneyspace

>   You can set anything

```XML
<intent-filter android:label="@string/app_name">
     <action android:name="android.intent.action.VIEW" />
     <category android:name="android.intent.category.DEFAULT" />
     <category android:name="android.intent.category.BROWSABLE" />
     <data android:scheme="payment" android:host="moneyspace" />
</intent-filter>
```

# MoneySpaceSDK

| Parameter | Description |Requred | 
|--|--|--|
| secret_id | secret_id will be generated fromWebhook’s menu in the merchant portal  |x|
| secret_key | secret_key will be generated fromWebhook’s menu in the merchant portal  |x|
| firstname | Show firstname on receipt  |x|
| lastname | Show lastname on receipt  |x|
| email | Email for receipt  |/|
| phone | -  |x|
| amount | Amount to be paid ( Must be 2 decimal place and no comma in numbers. )  |/|
| description | Product or service description  |x|
| address | Customer address  |x|
| message | instructions to merchant  |x|
| feeType | who pays fees ( Merchant  : include & Shopper  : exclude ) * qrprom can't use exclude |/|
| order_id| order id , Uppercase letters,number and cannot be repeated ( Maximum 20 characters ) |/|
| payment_type | card : Card 3D secured , qrprom : Pay by QR Code Promptpay  , qrnone : Image QR Code |/|
| success_Url |  As payment success, it will  redirect  to the specific  url.  |/|
| fail_Url | As payment failed, it will  redirect  to the specific  url.  |/|
| cancel_Url | As payment canceled, it will  redirect  to the specific  url.  |/|

---
###  **Example : Create transaction ID**

![enter image description here](https://worldwallet.net/android/icon/icon_java.png) **Java :**

```Java
Create_TransactionID MS_PAYMENT = new Create_TransactionID();
MS_PAYMENT.context = MoneySpace.this;
MS_PAYMENT.secret_id = "";
MS_PAYMENT.secret_key = "";
MS_PAYMENT.firstname = "";
MS_PAYMENT.lastname = "";
MS_PAYMENT.email = "";
MS_PAYMENT.phone = "";
MS_PAYMENT.amount = "100.00";
MS_PAYMENT.description = "";
MS_PAYMENT.address = "";
MS_PAYMENT.message = "";
MS_PAYMENT.feeType = "include";
MS_PAYMENT.order_id = "";
MS_PAYMENT.payment_type = "card";
MS_PAYMENT.success_Url = "payment://moneyspace"; // Deeplink
MS_PAYMENT.cancel_Url = "payment://moneyspace"; // Deeplink
MS_PAYMENT.fail_Url = "payment://moneyspace"; // Deeplink
MS_PAYMENT.execute();
```

 
```Java
public class Create_TransactionID extends AsyncTask< String,String,String> {  
  
    private Context context;private String secret_id,secret_key,firstname,lastname,email,phone,amount,description,address,message,feeType,order_id,payment_type,success_Url,cancel_Url,fail_Url;  
  
    @Override  
    protected String doInBackground(String... data) {  
        String res = null;  
        try {  
            res = MoneySpaceSDK.Payment(MoneySpaceSDK.PaymentParams(secret_id, secret_key, firstname, lastname, email, phone, amount, description, address, message, feeType, order_id, payment_type, success_Url, fail_Url, cancel_Url));  
            return res;  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
        return res;  
    }  
  
    @Override  
	protected void onPostExecute(String result) {  
        MoneySpaceSDK.Paynow(MoneySpace.this,result) // Open the payment link function
    }  
}
```
              
     
   ****
    
![enter image description here](https://worldwallet.net/android/icon/icon_kotlin.png) **Kotlin :** 

```Kotlin
val MS_PAYMENT = Create_TransactionID()
MS_PAYMENT.context = this@MoneySpace
MS_PAYMENT.secret_id = ""
MS_PAYMENT.secret_key = ""
MS_PAYMENT.firstname = ""
MS_PAYMENT.lastname = ""
MS_PAYMENT.email = ""
MS_PAYMENT.phone = ""
MS_PAYMENT.amount = "100.00"
MS_PAYMENT.description = ""
MS_PAYMENT.address = ""
MS_PAYMENT.message = ""
MS_PAYMENT.feeType = "include"
MS_PAYMENT.order_id = ""
MS_PAYMENT.payment_type = "card"
MS_PAYMENT.success_Url = "payment://moneyspace"; // Deeplink
MS_PAYMENT.cancel_Url = "payment://moneyspace"; // Deeplink
MS_PAYMENT.fail_Url = "payment://moneyspace"; // Deeplink
MS_PAYMENT.execute()
    
``` 

```Kotlin
inner class Create_TransactionID : AsyncTask<String, String, String>() {  
  
    var context: Context? = null  
    var secret_id: String? = null  
    var secret_key: String? = null  
    var firstname: String? = null  
    var lastname: String? = null  
    var email: String? = null  
    var phone: String? = null  
    var amount: String? = null  
    var description: String? = null  
    var address: String? = null  
    var message: String? = null  
    var feeType: String? = null  
    var order_id: String? = null  
    var payment_type: String? = null  
    var success_Url: String? = null  
    var cancel_Url: String? = null  
    var fail_Url: String? = null  
  
    override fun doInBackground(vararg data: String): String? {  
        var res: String? = null  
        try {  
            res = MoneySpaceSDK.Payment(MoneySpaceSDK.PaymentParams(secret_id, secret_key, firstname, lastname, email, phone, amount, description, address, message, feeType, order_id, payment_type , success_Url, fail_Url, cancel_Url))  
            return res  
        } catch (e: Exception) {  
            e.printStackTrace()  
        }  
  
        return res  
    }  
  
  
  
    override fun onPostExecute(result: String) {  
  
		MoneySpaceSDK.Paynow(this@MoneySpace, result) // Open the payment link function
  
    }  
}
```
****
### **Example : Check transaction ID**

| Parameter | Description |Requred | 
|--|--|--|
| secret_id | secret_id will be generated fromWebhook’s menu in the merchant portal  |/|
| secret_key | secret_key will be generated fromWebhook’s menu in the merchant portal  |/|
| TransactionID |   |/|

### JSON result
```json
[
	  {
    		"Amount ": "",
    		"Transaction ID ": "",
    		"Description ": "",
    		"Status Payment ": ""
	  }
]
```

###  Status of payment

| Status | Description |
|--|--|
| OK | creating a successful payment method |
| pending | waiting for payment |
| paysuccess |  payment success |
| fail |  payment failed |
| cancel |  payment canceled |


****

![enter image description here](https://worldwallet.net/android/icon/icon_java.png) **Java :**
```Java
Check_TransactionID MS_CHECK_TRANSACTION = new Check_TransactionID();
MS_CHECK_TRANSACTION.context = MoneySpace.this;
MS_CHECK_TRANSACTION.secret_id = "";
MS_CHECK_TRANSACTION.secret_key = "";
MS_CHECK_TRANSACTION.transaction_ID = "MSTRF18000000000000";
MS_CHECK_TRANSACTION.execute();
```

```Java
public class Check_TransactionID extends AsyncTask<String,String,String> {

        private Context context;private String secret_id,secret_key,transaction_ID;

        @Override
        protected String doInBackground(String... data) {
            String res = null;
            try {
                res = MoneySpaceSDK.TransactionID(MoneySpaceSDK.TransactionParams(secret_id, secret_key,transaction_ID));
                return res;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return res;
        }

        @Override
        protected void onPostExecute(String result) {

                // json result

        }
}
```
****

![enter image description here](https://worldwallet.net/android/icon/icon_kotlin.png) **Kotlin :** 
```Kotlin
var MS_CHECK_TRANSACTION = Check_TransactionID();
MS_CHECK_TRANSACTION.context = this@MoneySpace
MS_CHECK_TRANSACTION.secret_id = "";
MS_CHECK_TRANSACTION.secret_key = "";
MS_CHECK_TRANSACTION.transaction_ID = "MSTRF18000000000000";
MS_CHECK_TRANSACTION.execute();
```
```Kotlin
inner class Check_TransactionID : AsyncTask < String, String, String>() {

         var context: Context? = null
         var secret_id: String? = null
         var secret_key: String? = null
         var transaction_ID: String? = null

        override fun doInBackground(vararg data: String): String? {
            var res: String? = null
            try {
                res = MoneySpaceSDK.TransactionID(MoneySpaceSDK.TransactionParams(secret_id, secret_key, transaction_ID))
                return res
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return res
        }

        override fun onPostExecute(result: String) {

            // json result

        }
}
```
****



### Example : Redirecting redirect to app and receive transaction ID values ​​after successful payment

**Transaction ID** : Parameter  " **Ref** "

![enter image description here](https://worldwallet.net/android/icon/icon_java.png) **Java :**
```Java
Intent intent = getIntent();if(intent != null && intent.getData() != null){
      String transaction_ID = intent.getData().getQueryParameter("Ref");
      Toast.makeText(getApplicationContext(),"Transaction ID : " + transaction_ID,Toast.LENGTH_SHORT).show();
}
```
****
![enter image description here](https://worldwallet.net/android/icon/icon_kotlin.png) **Kotlin :** 
```Kotlin
val intent = intent
if (intent != null && intent.data != null) {
      var transaction_ID = intent.data!!.getQueryParameter("Ref")
      Toast.makeText(applicationContext, "Transaction ID : " + transaction_ID,Toast.LENGTH_SHORT).show()
}
```

****


# Webhook parameter


**Parameters when create transaction ID**

- TransactionID
- amount
- status
- orderid
- hash

****
**Parameters when payment is completed**

- TransectionID
- amount
- status
- orderid
- hash

****

# Changelog
- 2019-12-01 : Add new QR Code Promptpay and example code
- 2019-11-23 : Update README
- 2019-11-14 : Update QR Promtpay
- 2019-09-23 : Added