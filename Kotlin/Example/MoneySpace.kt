package com.example.democardjava

import android.content.Context
import org.junit.runner.RunWith
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.democardjava.R
import android.widget.EditText
import com.example.democardjava.MoneySpace.Create_TransactionID
import com.example.democardjava.MoneySpace
import android.widget.Toast
import android.content.Intent
import com.example.democardjava.MoneySpace.Check_TransactionID
import android.os.AsyncTask
import com.example.democardjava.MoneySpaceSDK
import org.json.JSONArray
import org.json.JSONObject
import com.example.democardjava.MoneySpace.DownLoadImageTask
import org.json.JSONException
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.View
import android.widget.Button
import android.widget.ImageView
import java.lang.Exception
import java.lang.StringBuilder
import java.net.URL
import java.util.*
import kotlin.Throws

class MoneySpace : AppCompatActivity() {
    private val secret_id: String? = ""
    private val secret_key: String? = ""
    var transaction_ID: String? = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_money_space)
        val paymentButton = findViewById<View?>(R.id.MoneyspacePayment) as Button
        val checkButton = findViewById<View?>(R.id.MoneyspaceCheck) as Button
        paymentButton.setOnClickListener {
            val amount = findViewById<View?>(R.id.amount) as EditText
            val MS_PAYMENT = Create_TransactionID()
            MS_PAYMENT.context = this@MoneySpace
            MS_PAYMENT.secret_id = secret_id
            MS_PAYMENT.secret_key = secret_key
            MS_PAYMENT.firstname = ""
            MS_PAYMENT.lastname = ""
            MS_PAYMENT.email = ""
            MS_PAYMENT.phone = ""
            MS_PAYMENT.amount = amount.text.toString()
            MS_PAYMENT.description = ""
            MS_PAYMENT.address = ""
            MS_PAYMENT.message = ""
            MS_PAYMENT.feeType = "include"
            MS_PAYMENT.order_id = "SP23032020967" + getRandomString(10)
            MS_PAYMENT.payment_type = "qrnone"
            MS_PAYMENT.success_Url = "payment://moneyspace" // กลับมายังแอพ
            MS_PAYMENT.cancel_Url = "payment://moneyspace" // กลับมายังแอพ
            MS_PAYMENT.fail_Url = "payment://moneyspace" // กลับมายังแอพ
            MS_PAYMENT.execute()
            paymentButton.isEnabled = false
            paymentButton.text = "Created"
            checkButton.isEnabled = true
            Toast.makeText(applicationContext, "Creating...", Toast.LENGTH_SHORT).show()
        }
        val intent = intent
        checkButton.setOnClickListener {
            if (transaction_ID !== "") {
                val MS_CHECK_TRANSACTION = Check_TransactionID()
                MS_CHECK_TRANSACTION.context = this@MoneySpace
                MS_CHECK_TRANSACTION.secret_id = secret_id
                MS_CHECK_TRANSACTION.secret_key = secret_key
                MS_CHECK_TRANSACTION.transaction_ID = transaction_ID
                MS_CHECK_TRANSACTION.execute()
            } else {
                Toast.makeText(applicationContext, "Not found TransactionID", Toast.LENGTH_SHORT)
                    .show()
            }
        }
        if (intent != null && intent.data != null) {
            val transaction_ID = intent.data.getQueryParameter("Ref")
            Toast.makeText(
                applicationContext,
                "ชำระเงินสำเร็จ : $transaction_ID",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    inner class Create_TransactionID : AsyncTask<String?, String?, String?>() {
        private val context: Context? = null
        private val secret_id: String? = null
        private val secret_key: String? = null
        private val firstname: String? = null
        private val lastname: String? = null
        private val email: String? = null
        private val phone: String? = null
        private val amount: String? = null
        private val description: String? = null
        private val address: String? = null
        private val message: String? = null
        private val feeType: String? = null
        private val order_id: String? = null
        private val payment_type: String? = null
        private val success_Url: String? = null
        private val fail_Url: String? = null
        private val cancel_Url: String? = null
        override fun doInBackground(vararg data: String?): String? {
            var res: String? = null
            try {
                res = MoneySpaceSDK.Payment(
                    MoneySpaceSDK.PaymentParams(
                        secret_id,
                        secret_key,
                        firstname,
                        lastname,
                        email,
                        phone,
                        amount,
                        description,
                        address,
                        message,
                        feeType,
                        order_id,
                        payment_type,
                        success_Url,
                        fail_Url,
                        cancel_Url
                    )
                )
                return res
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return res
        }

        override fun onPostExecute(result: String?) {
            if (result != null) {
                try {
                    val jsonarray = JSONArray(result)
                    val obj = jsonarray.getJSONObject(0)
                    val status = obj.getString("status")
                    if (status != null && status == "success") {
                        val Tx = obj.getString("transaction_ID")
                        val QRLINK = obj.getString("image_qrprom")
                        transaction_ID = Tx
                        Toast.makeText(applicationContext, "Success : $Tx", Toast.LENGTH_SHORT)
                            .show()
                        if (QRLINK.length > 0) {
                            DownLoadImageTask(findViewById<View?>(R.id.qr) as ImageView).execute(
                                QRLINK
                            )
                        } else if (MoneySpaceSDK.Paynow(this@MoneySpace, result) != "opened") {
                            Toast.makeText(applicationContext, "Error", Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        Toast.makeText(
                            applicationContext,
                            "Failed : $jsonarray",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: JSONException) {
                }
            }
        }
    }

    inner class Check_TransactionID : AsyncTask<String?, String?, String?>() {
        private val context: Context? = null
        private val secret_id: String? = null
        private val secret_key: String? = null
        private val transaction_ID: String? = null
        override fun doInBackground(vararg data: String?): String? {
            var res: String? = null
            try {
                res = MoneySpaceSDK.TransactionID(
                    MoneySpaceSDK.TransactionParams(
                        secret_id,
                        secret_key,
                        transaction_ID
                    )
                )
                if (res != null) {
                    return try {
                        val jsonarray = JSONArray(res)
                        val obj = jsonarray.getJSONObject(0)
                        obj.getString("Status Payment ")
                    } catch (e: JSONException) {
                        "error"
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return res
        }

        override fun onPostExecute(result: String?) {
            Toast.makeText(applicationContext, "Status Payment : $result", Toast.LENGTH_SHORT)
                .show()
        }
    }

    private inner class DownLoadImageTask(var imageView: ImageView?) :
        AsyncTask<String?, Void?, Bitmap?>() {
        override fun doInBackground(vararg urls: String?): Bitmap? {
            val urlOfImage = urls[0]
            var logo: Bitmap? = null
            try {
                val `is` = URL(urlOfImage).openStream()
                logo = BitmapFactory.decodeStream(`is`)
            } catch (e: Exception) { // Catch the download exception
                e.printStackTrace()
            }
            return logo
        }

        override fun onPostExecute(result: Bitmap?) {
            imageView.setImageBitmap(result)
        }
    }

    companion object {
        private val ALLOWED_CHARACTERS: String? = "0123456789qwertyuiopasdfghjklzxcvbnm"
        private fun getRandomString(sizeOfRandomString: Int): String? {
            val random = Random()
            val sb = StringBuilder(sizeOfRandomString)
            for (i in 0 until sizeOfRandomString) sb.append(
                ALLOWED_CHARACTERS.get(
                    random.nextInt(
                        ALLOWED_CHARACTERS.length
                    )
                )
            )
            return sb.toString()
        }
    }
}