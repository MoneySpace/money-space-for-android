package com.example.democardjava

import android.content.Context
import org.junit.runner.RunWith
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.democardjava.R
import android.widget.EditText
import com.example.democardjava.MoneySpace.Create_TransactionID
import com.example.democardjava.MoneySpace
import android.widget.Toast
import android.content.Intent
import com.example.democardjava.MoneySpace.Check_TransactionID
import android.os.AsyncTask
import com.example.democardjava.MoneySpaceSDK
import org.json.JSONArray
import org.json.JSONObject
import com.example.democardjava.MoneySpace.DownLoadImageTask
import org.json.JSONException
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import java.io.BufferedReader
import java.io.BufferedWriter
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.lang.Exception
import java.lang.StringBuilder
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder
import javax.net.ssl.HttpsURLConnection
import kotlin.Throws

object MoneySpaceSDK {
    private val baseurl: String? = "https://a.moneyspace.net/"
    @Throws(Exception::class)
    fun Payment(postDataParams: String?): String? {
        val url = URL(baseurl + "payment/CreateTransaction")
        val conn = url.openConnection() as HttpURLConnection
        conn.readTimeout = 20000
        conn.connectTimeout = 20000
        conn.requestMethod = "POST"
        conn.doInput = true
        conn.doOutput = true
        val os = conn.outputStream
        val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
        writer.write(postDataParams)
        writer.flush()
        writer.close()
        os.close()
        val responseCode = conn.responseCode // To Check for 200
        if (responseCode == HttpsURLConnection.HTTP_OK) {
            val `in` = BufferedReader(InputStreamReader(conn.inputStream))
            val sb = StringBuffer("")
            var line: String? = ""
            while (`in`.readLine().also { line = it } != null) {
                sb.append(line)
                break
            }
            `in`.close()
            return sb.toString()
        }
        return null
    }

    @Throws(Exception::class)
    fun TransactionID(transactionID: String?): String? {
        val url = URL(baseurl + "payment/transactionID/")
        val conn = url.openConnection() as HttpURLConnection
        conn.readTimeout = 20000
        conn.connectTimeout = 20000
        conn.requestMethod = "POST"
        conn.doInput = true
        conn.doOutput = true
        val os = conn.outputStream
        val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
        writer.write(transactionID)
        writer.flush()
        writer.close()
        os.close()
        val responseCode = conn.responseCode // To Check for 200
        if (responseCode == HttpsURLConnection.HTTP_OK) {
            val `in` = BufferedReader(InputStreamReader(conn.inputStream))
            val sb = StringBuffer("")
            var line: String? = ""
            while (`in`.readLine().also { line = it } != null) {
                sb.append(line)
                break
            }
            `in`.close()
            return sb.toString()
        }
        return null
    }

    @Throws(Exception::class)
    fun encodeParams(params: JSONObject?): String? {
        val result = StringBuilder()
        var first = true
        val itr = params.keys()
        while (itr.hasNext()) {
            val key = itr.next()
            val value = params.get(key)
            if (first) first = false else result.append("&")
            result.append(URLEncoder.encode(key, "UTF-8"))
            result.append("=")
            result.append(URLEncoder.encode(value.toString(), "UTF-8"))
        }
        return result.toString()
    }

    fun OpenPayment(context: Context?, url: String?) {
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        context.startActivity(i)
    }

    fun PaymentParams(
        secret_id: String?,
        secret_key: String?,
        firstname: String?,
        lastname: String?,
        email: String?,
        phone: String?,
        amount: String?,
        description: String?,
        address: String?,
        message: String?,
        feeType: String?,
        order_id: String?,
        payment_type: String?,
        success_Url: String?,
        fail_Url: String?,
        cancel_Url: String?
    ): String? {
        return try {
            val PaymentParams = JSONObject()
            PaymentParams.put("secret_id", secret_id)
            PaymentParams.put("secret_key", secret_key)
            PaymentParams.put("firstname", firstname)
            PaymentParams.put("lastname", lastname)
            PaymentParams.put("email", email)
            PaymentParams.put("phone", phone)
            PaymentParams.put("amount", amount)
            PaymentParams.put("currency", "THB")
            PaymentParams.put("description", description)
            PaymentParams.put("address", address)
            PaymentParams.put("message", message)
            PaymentParams.put("feeType", feeType)
            PaymentParams.put("order_id", order_id)
            PaymentParams.put("payment_type", payment_type)
            PaymentParams.put("success_Url", success_Url)
            PaymentParams.put("fail_Url", fail_Url)
            PaymentParams.put("cancel_Url", cancel_Url)
            encodeParams(PaymentParams)
        } catch (e: Exception) {
            String(e.message)
        }
    }

    fun TransactionParams(
        secret_id: String?,
        secret_key: String?,
        transaction_ID: String?
    ): String? {
        return try {
            val PaymentParams = JSONObject()
            PaymentParams.put("secret_id", secret_id)
            PaymentParams.put("secret_key", secret_key)
            PaymentParams.put("transaction_ID", transaction_ID)
            encodeParams(PaymentParams)
        } catch (e: Exception) {
            String(e.message)
        }
    }

    fun Paynow(context: Context?, JSONdata: String?): String? {
        return if (JSONdata != null) {
            try {
                val jsonarray = JSONArray(JSONdata)
                val obj = jsonarray.getJSONObject(0)
                val status = obj.getString("status")
                if (status != null && status == "success") {
                    val linkpayment = obj.getString("link_payment")
                    OpenPayment(context, linkpayment)
                    "opened"
                } else {
                    "error"
                }
            } catch (e: JSONException) {
                "error"
            }
        } else null
    }
}