package com.example.democardjava

import org.junit.runner.RunWith
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.democardjava.R
import android.widget.EditText
import com.example.democardjava.MoneySpace.Create_TransactionID
import com.example.democardjava.MoneySpace
import android.widget.Toast
import android.content.Intent
import com.example.democardjava.MoneySpace.Check_TransactionID
import android.os.AsyncTask
import com.example.democardjava.MoneySpaceSDK
import org.json.JSONArray
import org.json.JSONObject
import com.example.democardjava.MoneySpace.DownLoadImageTask
import org.json.JSONException
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.View
import android.widget.Button
import kotlin.Throws

class MainActivity : AppCompatActivity() {
    var test_creditcard: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        test_creditcard = findViewById(R.id.test_creditcard)
        test_creditcard.setOnClickListener(View.OnClickListener {
            val mIntent = Intent(this@MainActivity, MoneySpace::class.java)
            startActivity(mIntent)
        })
    }
}