package com.example.democardjava;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

public class MoneySpaceSDK {

    private static String baseurl = "https://a.moneyspace.net/";

    public static String Payment(String postDataParams) throws Exception {

        URL url = new URL(baseurl + "payment/CreateTransaction");

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(20000);
        conn.setConnectTimeout(20000);
        conn.setRequestMethod("POST");
        conn.setDoInput(true);
        conn.setDoOutput(true);


        OutputStream os = conn.getOutputStream();
        BufferedWriter writer = new BufferedWriter( new OutputStreamWriter(os, "UTF-8"));
        writer.write(postDataParams);
        writer.flush();
        writer.close();
        os.close();

        int responseCode=conn.getResponseCode(); // To Check for 200
        if (responseCode == HttpsURLConnection.HTTP_OK) {

            BufferedReader in=new BufferedReader( new InputStreamReader(conn.getInputStream()));
            StringBuffer sb = new StringBuffer("");
            String line="";
            while((line = in.readLine()) != null) {
                sb.append(line);
                break;
            }
            in.close();
            return sb.toString();
        }
        return null;
    }

    public static String TransactionID(String transactionID) throws Exception {
        URL url = new URL(baseurl + "payment/transactionID/");

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(20000);
        conn.setConnectTimeout(20000);
        conn.setRequestMethod("POST");
        conn.setDoInput(true);
        conn.setDoOutput(true);


        OutputStream os = conn.getOutputStream();
        BufferedWriter writer = new BufferedWriter( new OutputStreamWriter(os, "UTF-8"));
        writer.write(transactionID);
        writer.flush();
        writer.close();
        os.close();

        int responseCode=conn.getResponseCode(); // To Check for 200
        if (responseCode == HttpsURLConnection.HTTP_OK) {

            BufferedReader in=new BufferedReader( new InputStreamReader(conn.getInputStream()));
            StringBuffer sb = new StringBuffer("");
            String line="";
            while((line = in.readLine()) != null) {
                sb.append(line);
                break;
            }
            in.close();
            return sb.toString();
        }
        return null;
    }

    public static String encodeParams(JSONObject params) throws Exception {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        Iterator<String> itr = params.keys();
        while(itr.hasNext()){
            String key= itr.next();
            Object value = params.get(key);
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));
        }
        return result.toString();
    }

    public static void OpenPayment(Context context, String url){
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        context.startActivity(i);
    }

    public static String PaymentParams(String secret_id,String secret_key,String firstname,String lastname, String email, String phone, String amount, String description, String address, String message,String feeType ,String order_id, String payment_type , String success_Url, String fail_Url, String cancel_Url){
        try {

            JSONObject PaymentParams = new JSONObject();
            PaymentParams.put("secret_id", secret_id);
            PaymentParams.put("secret_key", secret_key);
            PaymentParams.put("firstname", firstname);
            PaymentParams.put("lastname", lastname);
            PaymentParams.put("email", email);
            PaymentParams.put("phone", phone);
            PaymentParams.put("amount", amount);
            PaymentParams.put("currency", "THB");
            PaymentParams.put("description", description);
            PaymentParams.put("address", address);
            PaymentParams.put("message", message);
            PaymentParams.put("feeType", feeType);
            PaymentParams.put("order_id", order_id);
            PaymentParams.put("payment_type", payment_type);
            PaymentParams.put("success_Url", success_Url);
            PaymentParams.put("fail_Url", fail_Url);
            PaymentParams.put("cancel_Url", cancel_Url);

            return encodeParams(PaymentParams);
        }
        catch(Exception e){
            return new String(e.getMessage());
        }
    }

    public static String TransactionParams(String secret_id,String secret_key,String transaction_ID){
        try {

            JSONObject PaymentParams = new JSONObject();
            PaymentParams.put("secret_id", secret_id);
            PaymentParams.put("secret_key", secret_key);
            PaymentParams.put("transaction_ID", transaction_ID);


            return encodeParams(PaymentParams);
        }
        catch(Exception e){
            return new String(e.getMessage());
        }
    }

    public static String Paynow(Context context,String JSONdata){

        if(JSONdata!=null){
            try {
                JSONArray jsonarray = new JSONArray(JSONdata);
                JSONObject obj = jsonarray.getJSONObject(0);
                String status = obj.getString("status");
                if (status != null && status.equals("success")){
                    String linkpayment = obj.getString("link_payment");
                    OpenPayment(context,linkpayment);
                    return "opened";
                }else{
                    return "error";
                }

            } catch (JSONException e) {
                return "error";
            }

        }
        return null;
    }


}