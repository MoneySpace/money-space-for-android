package com.example.democardjava;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.util.Random;

public class MoneySpace extends AppCompatActivity {

    private String secret_id = "";
    private String secret_key = "";



    public String transaction_ID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_money_space);
        final Button paymentButton = (Button) findViewById(R.id.MoneyspacePayment);
        final Button checkButton = (Button) findViewById(R.id.MoneyspaceCheck);

        paymentButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText amount = (EditText) findViewById(R.id.amount);

                Create_TransactionID MS_PAYMENT = new Create_TransactionID();
                MS_PAYMENT.context = MoneySpace.this;
                MS_PAYMENT.secret_id = secret_id;
                MS_PAYMENT.secret_key = secret_key;
                MS_PAYMENT.firstname = "";
                MS_PAYMENT.lastname = "";
                MS_PAYMENT.email = "";
                MS_PAYMENT.phone = "";
                MS_PAYMENT.amount = amount.getText().toString();
                MS_PAYMENT.description = "";
                MS_PAYMENT.address = "";
                MS_PAYMENT.message = "";
                MS_PAYMENT.feeType = "include";
                MS_PAYMENT.order_id = "SP23032020967" + getRandomString(10);
                MS_PAYMENT.payment_type = "qrnone";
                MS_PAYMENT.success_Url = "payment://moneyspace"; // กลับมายังแอพ
                MS_PAYMENT.cancel_Url = "payment://moneyspace"; // กลับมายังแอพ
                MS_PAYMENT.fail_Url = "payment://moneyspace"; // กลับมายังแอพ
                MS_PAYMENT.execute();

                paymentButton.setEnabled(false);
                paymentButton.setText("Created");
                checkButton.setEnabled(true);
                Toast.makeText(getApplicationContext(),"Creating...",Toast.LENGTH_SHORT).show();

            }
        });




        Intent intent = getIntent();

        checkButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (transaction_ID != ""){
                    Check_TransactionID MS_CHECK_TRANSACTION = new Check_TransactionID();
                    MS_CHECK_TRANSACTION.context = MoneySpace.this;
                    MS_CHECK_TRANSACTION.secret_id = secret_id;
                    MS_CHECK_TRANSACTION.secret_key = secret_key;
                    MS_CHECK_TRANSACTION.transaction_ID = transaction_ID;
                    MS_CHECK_TRANSACTION.execute();
                }else{
                    Toast.makeText(getApplicationContext(),"Not found TransactionID",Toast.LENGTH_SHORT).show();
                }

            }
        });

        if(intent != null && intent.getData() != null){
            String transaction_ID = intent.getData().getQueryParameter("Ref");
            Toast.makeText(getApplicationContext(),"ชำระเงินสำเร็จ : " + transaction_ID,Toast.LENGTH_SHORT).show();
        }



    }

    public class Create_TransactionID extends AsyncTask< String,String,String> {

        private Context context;private String secret_id,secret_key,firstname,lastname,email,phone,amount,description,address,message,feeType,order_id,payment_type,success_Url,fail_Url,cancel_Url;

        @Override
        protected String doInBackground(String... data) {
            String res = null;
            try {
                res = MoneySpaceSDK.Payment(MoneySpaceSDK.PaymentParams(secret_id, secret_key, firstname, lastname, email, phone, amount, description, address, message, feeType, order_id,payment_type, success_Url, fail_Url, cancel_Url));
                return res;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return res;
        }

        @Override
        protected void onPostExecute(String result) {



            if(result!=null){
                try {
                    JSONArray jsonarray = new JSONArray(result);
                    JSONObject obj = jsonarray.getJSONObject(0);
                    String status = obj.getString("status");

                    if (status != null && status.equals("success")){

                        String Tx = obj.getString("transaction_ID");
                        String QRLINK = obj.getString("image_qrprom");
                        transaction_ID = Tx;
                        Toast.makeText(getApplicationContext(),"Success : " + Tx ,Toast.LENGTH_SHORT).show();

                        if(QRLINK.length() > 0){
                            new DownLoadImageTask((ImageView) findViewById(R.id.qr)).execute(QRLINK);
                        }else if (!MoneySpaceSDK.Paynow(MoneySpace.this,result).equals("opened")){
                            Toast.makeText(getApplicationContext(),"Error",Toast.LENGTH_SHORT).show();
                        }



                    }else{
                        Toast.makeText(getApplicationContext(),"Failed : " + jsonarray ,Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {

                }

            }


        }
    }

    public class Check_TransactionID extends AsyncTask<String,String,String> {

        private Context context;private String secret_id,secret_key,transaction_ID;

        @Override
        protected String doInBackground(String... data) {
            String res = null;
            try {
                res = MoneySpaceSDK.TransactionID(MoneySpaceSDK.TransactionParams(secret_id, secret_key,transaction_ID));

                if(res!=null){
                    try {
                        JSONArray jsonarray = new JSONArray(res);
                        JSONObject obj = jsonarray.getJSONObject(0);
                        String status = obj.getString("Status Payment ");
                        return status;

                    } catch (JSONException e) {
                        return "error";
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return res;
        }

        @Override
        protected void onPostExecute(String result) {

            Toast.makeText(getApplicationContext(),"Status Payment : "+result,Toast.LENGTH_SHORT).show();

        }
    }
    private static final String ALLOWED_CHARACTERS ="0123456789qwertyuiopasdfghjklzxcvbnm";

    private static String getRandomString(final int sizeOfRandomString)
    {
        final Random random=new Random();
        final StringBuilder sb=new StringBuilder(sizeOfRandomString);
        for(int i=0;i<sizeOfRandomString;++i)
            sb.append(ALLOWED_CHARACTERS.charAt(random.nextInt(ALLOWED_CHARACTERS.length())));
        return sb.toString();
    }
    private class DownLoadImageTask extends AsyncTask<String,Void,Bitmap>{
        ImageView imageView;

        public DownLoadImageTask(ImageView imageView){
            this.imageView = imageView;
        }

        protected Bitmap doInBackground(String...urls){
            String urlOfImage = urls[0];
            Bitmap logo = null;
            try{
                InputStream is = new URL(urlOfImage).openStream();

                logo = BitmapFactory.decodeStream(is);
            }catch(Exception e){ // Catch the download exception
                e.printStackTrace();
            }
            return logo;
        }


        protected void onPostExecute(Bitmap result){
            imageView.setImageBitmap(result);
        }
    }


}
